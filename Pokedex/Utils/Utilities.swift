//
//  Utilities.swift
//  Pokedex
//
//  Created by Camilo Soto on 13/12/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import UIKit

class Utilities: NSObject{
    
    static func showAlert(msg: String, controller: UIViewController) {
        let alert = UIAlertController(
            title: "Alerta",
            message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    
    static func getImageType(idType: Int64) -> UIImage{
        switch idType {
        case 1:
            return UIImage(named: "normal")!
            
        case 2:
            return UIImage(named: "poison")!
            
        case 3:
            return UIImage(named: "flying")!
                
        case 4:
            return UIImage(named: "poison")!
            
        case 5:
            return UIImage(named: "ground")!
                
        case 6:
            return UIImage(named: "poison")!
                
        case 7:
            return UIImage(named: "bug")!
                    
        case 8:
            return UIImage(named: "electric")!
            
        case 9:
            return UIImage(named: "normal")!
                
        case 10:
            return UIImage(named: "fire")!
                
        case 11:
            return UIImage(named: "water")!
                    
        case 12:
            return UIImage(named: "grass")!
            
        case 18:
            return UIImage(named: "fairy")!
            
        default:
            return UIImage(named: "rock")!
        }
    }
}
