//
//  Queries.swift
//  Pokedex
//
//  Created by Soto Rodriguez on 12/13/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import Foundation


let queue = DBManager.SharedInstance.getDBQueue()

class Queries: NSObject {

    //MARK: Insertar
    // Insertar lista de Pokemon
    class func insertPokemons(listPokemons: [Pokemon]){
        for pokemon in listPokemons {
            let pokemonTable = PokemonDB(idPokemon: pokemon.id, name: pokemon.name, description: "", id_type_1: 0, id_type_2: 0, url_image: pokemon.urlImage)
            
            if getPokemonForId(id: pokemon.id) == nil {
                try! queue.inTransaction{db in
                    try! pokemonTable.insert(db)
                    return .commit
                }
            }
            
        }
    }
    
    // Insertar tipo de pokemon
    class func insertType(type: TypesDB){
        
        if getTypeForID(id: type.idType!) == nil {
            try! queue.inTransaction{db in
                try! type.insert(db)
                return .commit
            }
        }
    }
    
    // Insertar estadisticas de pokemon
    class func insertStats(stat: StatsDB){
       
        if getStatForID(id: stat.idPokemon!) == nil {
            try! queue.inTransaction{db in
                try! stat.insert(db)
                return .commit
            }
        }
    }
    
    //MARK: Obtener
    
    // Obtener Pokemon por ID
    class func getPokemonForId(id: Int64) -> PokemonDB?{
        let pokemon = queue.inDatabase{db in
        PokemonDB.fetchOneWithQuery(db, "\(PokemonDao.ID_POKEMON) == \(id)")
        }
        return pokemon
    }
    
    // Obtener la lista de pokemon
    class func getAllPokemon() -> [Pokemon]{
        var listPokemon: [Pokemon] = []
        let pokemons = queue.inDatabase{db in
            PokemonDB.fetchAll(db: db)
        }
        
        for pokemon in pokemons {
            let item = Pokemon(id: pokemon.id, name: pokemon.name ?? "", urlDetail: "", urlImage: pokemon.url_image ?? "", type_1: pokemon.id_type_1!, type_2: pokemon.id_type_2!)
            listPokemon.append(item)
        }
        
        return listPokemon
    }
    
    // Obtener el tipo de pokemon por ID
    class func getTypeForID(id: Int64) -> TypesDB?{
        let type = queue.inDatabase{db in
            TypesDB.fetchOneWithQuery(db, "\(TypesDao.ID_TYPE) == \(id)")
        }
        return type
    }
    
    // Obtener  todos los tipos
    class func getAllTypes() -> [TypesDB]{
        let types = queue.inDatabase{db in
            TypesDB.fetchAll(db:db)
        }
        return types
    }
    
    // Obtener estadisticas por Id de pokemon
    class func getStatForID(id: Int64) -> StatsDB?{
        let stat = queue.inDatabase{db in
            StatsDB.fetchOneWithQuery(db, "\(StatsDao.ID) == \(id)")
        }
        
        return stat
    }
    
    //MARK: Update
    class func updateDescriptionPokemon(id: Int64, description: String){
        let pokemon = getPokemonForId(id: id)
        if pokemon != nil {
            pokemon!.description = description
            try! queue.inTransaction{db in
                try! pokemon!.update(db)
                return .commit
            }
        }
    }
    
    class func updateDetailPokemon(pokemondetail: DetailPokemon){
        let pokemon = getPokemonForId(id: pokemondetail.idPokemon)
        if pokemon != nil {
            pokemon!.id_type_1 = Int(pokemondetail.type_1)
            pokemon!.id_type_2 = Int(pokemondetail.type_2)
            try! queue.inTransaction{db in
                try! pokemon!.update(db)
                return .commit
            }
        }
    }
}
