//
//  HomeContract.swift
//  Pokedex
//
//  Created by Camilo Soto on 13/12/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import Foundation


protocol HomeContractView {
    func showPokemon(list: [Pokemon], index: Int, isSearch: Bool)
    func showError(error: String)
}

protocol HomeContractPresenter {
    func searchPokemon(txtSearch: String)
    func getMorePages()
    func refreshPage()
}

protocol HomeContractInteractor {
    //func getGenders()
    func getPokemons()
    func getMore()
    func searchPokemon(txtSearch: String)
    func setCallback(callback: HomeContractCallback)
    
}

protocol HomeContractCallback {
    func returnListPokemon(list: [Pokemon], index: Int, isSearch: Bool)
    func error(description: String)
}
