//
//  DBManager.swift
//  Magentrack
//
//  Created by Camilo Soto on 3/4/19.
//  Copyright © 2019 Pammos. All rights reserved.
//

import UIKit
import GRDB

class DBManager {
    
    static let SharedInstance = DBManager()
    fileprivate var dbQueue: DatabaseQueue
    
    fileprivate init() {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! as NSString
        let databasePath = documentsPath.appendingPathComponent("db_pokedex.sqlite")
        dbQueue = try! DatabaseQueue(path: databasePath)
    }
    
    func getDBQueue() -> DatabaseQueue {
        return dbQueue
    }
}
