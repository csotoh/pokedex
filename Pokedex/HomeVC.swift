//
//  HomeVC.swift
//  Pokedex
//
//  Created by Camilo Soto on 12/12/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import UIKit

class HomeVC: UIViewController, HomeContractView, UITableViewDelegate, UITableViewDataSource{
    
    
    
    @IBOutlet weak var tablePokemons: UITableView!
    @IBOutlet weak var lineView: UIView!
    
    var presenter: HomeContractPresenter?
    var listPokemon : [Pokemon] = []
    var index = 0
    var isSearch = false
    var pokemonSelect: Pokemon?
    let colorsBackground = [#colorLiteral(red: 0.8745098039, green: 0.9137254902, blue: 0.9803921569, alpha: 1),#colorLiteral(red: 0.8666666667, green: 0.9647058824, blue: 0.8549019608, alpha: 1)]
    let colorsLine = [#colorLiteral(red: 0.5623366469, green: 0.6496056489, blue: 0.9803921569, alpha: 1),#colorLiteral(red: 0.5549299556, green: 0.9803921569, blue: 0.8637423562, alpha: 1),#colorLiteral(red: 0.4739306041, green: 0.9803921569, blue: 0.4905084534, alpha: 1)]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tablePokemons.delegate = self
        self.tablePokemons.dataSource = self
        
        lineView.setGradientMultiColor(colorOne: colorsLine[0], colorTwo: colorsLine[1], colorThree: colorsLine[2])
        view.setGradientBackgroundColor(colorOne: colorsBackground[1], colorTwo: colorsBackground[0])
        
        presenter = HomePresenter(view: self, interactor: HomeInteractor())
        
        let types = Queries.getAllTypes()
    }
    
    //MARK: View
    func showPokemon(list: [Pokemon], index: Int, isSearch: Bool) {
        self.index = index
        self.listPokemon = list
        LoadingIndicator.hide()
        self.tablePokemons.reloadData {
             self.isSearch = isSearch
            if index > 0 {
                let lastIndexPath = IndexPath(row: index, section: 0)
                self.tablePokemons.scrollToRow(at: lastIndexPath, at: .bottom, animated: false)
            }
        }
    }
    
    func showError(error: String) {
        LoadingIndicator.hide()
        Utilities.showAlert(msg: error, controller: self)
    }
    
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listPokemon.count
    }

       
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
        let pokemon = listPokemon[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellList", for: indexPath) as! PokemonCell
        cell.initData(name: pokemon.name, urlImage: pokemon.urlImage, id: Int(pokemon.id), isSearch: self.isSearch, indexRow: indexPath.row, type_1: Int64(pokemon.type_1), type_2: Int64(pokemon.type_2))
       
        return cell
    }
       
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard !isSearch else {return}
        let totalRows = tableView.numberOfRows(inSection: indexPath.section)
        //first get total rows in that section by current indexPath.
        if indexPath.row == totalRows - 1 {
            presenter?.getMorePages()
        }
           
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        self.pokemonSelect = self.listPokemon[indexPath.row]
        performSegue(withIdentifier: "toDetail", sender: self)
    }
       
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetail"{
            let destination = segue.destination as! DetailVC
            destination.idPokemon = self.pokemonSelect!.id
        }
    }

}


