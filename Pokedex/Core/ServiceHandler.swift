//
//  ServiceHandler.swift
//  Pokedex
//
//  Created by Camilo Soto on 13/12/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import Foundation
import Alamofire

class ServiceHandler: NSObject {
    
    
    enum Service {
        case list_pokemons
        case pokemon_detail
        case specie
        case type
    }
    
    let POKEMONS = "/pokemon/"
    let POKEMON_DETAIL = "/pokemon/"
    let SPECIE = "/pokemon-species/"
    let TYPE = "/type/"
    
    
    let FAILURE = 2
    let SUCESS = 1
    let FAILURE_SERVER = 0
    
    
    var alamofireManager: SessionManager?
    
    override init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60
        
        alamofireManager = Alamofire.SessionManager(configuration: configuration)
        
    }
        
        
        // MARK: Metodo GET
        func doGetService(callback: ((_ isOk: Bool, _ mData: [AnyObject]) ->Void)?, service: Service, parameters:[String]) -> Void {
            
            var url: String!
            var parameter: Parameters = [:]
            
            
            switch service {
            case .list_pokemons:
                guard parameters.count > 0 else {return}
                url = POKEMONS
                parameter = [
                    "offset" : parameters[0],
                    "limit" : "50"
                ]
                
                
            case .pokemon_detail:
                guard parameters.count > 0 else {return}
                url = POKEMON_DETAIL + parameters[0]
               
                
            case .specie:
                guard parameters.count > 0 else {return}
                url = SPECIE + parameters[0]
                
            case .type:
               guard parameters.count > 0 else {return}
               url = TYPE + parameters[0]
                
            default:
                break
            }
            
            url = self.getBaseUrl() + url
            
            print("Request \(url) Parametros: \(parameter)")
            
            self.alamofireManager!.request(url, parameters: parameter)
                .responseJSON { response in switch response.result {
                case .success(let json):
                    
                    
                    if response.response?.statusCode == 200{
                        
                        if let post = self.processData(service, value: json as! NSDictionary) {
                            // to make sure it posted, print the results
                            callback?(true, post)
                        }
                    }else if response.response?.statusCode == 403{
                        callback?(false, ["El área en el que se encuentra, no tiene permiso para acceder al servidor (\(String(describing: response.response!.statusCode)))." as AnyObject])
                    }else{
                        callback?(false, ["Problemas al contactar con el servidor (\(String(describing: response.response!.statusCode)))." as AnyObject])
                    }
                    
                    
                case .failure(let error):
                    var errorStr = error.localizedDescription
                    if errorStr == "The request timed out."{
                        errorStr = "Sin respuesta del servidor, verifique su conexión a internet"
                    }
                    
                    callback?(false, [errorStr as AnyObject])
                    }
            }
        }
    
    
    
    // Proceso de la Data
    func processData(_ service: Service, value: NSDictionary) -> [AnyObject]? {
        
        let modelHandler = ModelHandler()
        
        switch service {
            
        case .list_pokemons:
            
            let listPokemon = modelHandler.ConvertListPokemon(data: value)
            Queries.insertPokemons(listPokemons: listPokemon)
            return [listPokemon as AnyObject]
            
        case .pokemon_detail:
            
            let pokemonDetail = modelHandler.ConvertPokemonDetail(data: value)
           
            Queries.insertStats(stat: pokemonDetail.1)
            return [pokemonDetail as AnyObject]
            
        case .specie:
            
            modelHandler.ConvertSpeciesPokemon(data: value)
            return ["Ok" as AnyObject]
            
        case .type:
            modelHandler.ConvertTypePokemon(data: value)
            return ["Ok" as AnyObject]
        }
        
        return [false as AnyObject, "Error de servicio" as AnyObject]
    }
    
    
    //MARK: URL base
    func getBaseUrl() -> String {
        return "https://pokeapi.co/api/v2"
    }
}
