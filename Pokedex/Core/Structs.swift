//
//  Structs.swift
//  Pokedex
//
//  Created by Camilo Soto on 13/12/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import Foundation


struct Pokemon {
    var id: Int64
    var name: String
    var urlDetail: String
    var urlImage: String
    var type_1: Int
    var type_2: Int
}

struct DetailPokemon{
    var idPokemon: Int64
    var type_1: Int64
    var type_2: Int64
}

struct Stats{
    var idPokemon: Int64
    var hp: Int
    var attack: Int
    var defense: Int
    var speed: Int
    var special_attack: Int
    var special_defense: Int
}
