//
//  ModelHandler.swift
//  Pokedex
//
//  Created by Camilo Soto on 13/12/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import Foundation

class ModelHandler: NSObject{
    
    func ConvertListPokemon(data: NSDictionary) -> [Pokemon]{
        
        var listPokemon: [Pokemon] = []
        
        let count = (data as AnyObject).object(forKey: "count") as! Int
        
        if let results = (data as AnyObject).object(forKey: "results") as? NSArray{
            for item in results{
                let name = (item as AnyObject).object(forKey: "name") as! String
                let url = (item as AnyObject).object(forKey: "url") as! String
                var urlImage = "https://pokeres.bastionbot.org/images/pokemon/1.png"
                
                let array = url.components(separatedBy: "/")
                var idPokemon: Int64 = 1
                if array.count > 2 {
                    idPokemon = Int64(array[array.count - 2]) ?? 1
                    urlImage = "https://pokeres.bastionbot.org/images/pokemon/\(idPokemon).png"
                }
                
                let pokemon = Pokemon(id: idPokemon, name: name.capitalized, urlDetail: url, urlImage: urlImage, type_1: 0, type_2: 0)
                listPokemon.append(pokemon)
            }
        }
        
        return listPokemon
    }
    
    func ConvertPokemonDetail(data: NSDictionary) -> (DetailPokemon, StatsDB){
        
        var detailPokemon = DetailPokemon(idPokemon: 0, type_1: 0, type_2: 0)
        var statsObj = StatsDB(idPokemon: 0, hp: 0, attack: 0, defense: 0, speed: 0, special_attack: 0, special_defense: 0)
        var listTypes: [Int64] = []
        
        let id = (data as AnyObject).object(forKey: "id") as! Int64
        
        if let types = (data as AnyObject).object(forKey: "types") as? NSArray{
            for item in types{
                if let type = (item as AnyObject).object(forKey: "type") as? NSDictionary{
                    let url = (type as AnyObject).object(forKey: "url") as! String
                    
                    let array = url.components(separatedBy: "/")
                    let idType = Int64(array[array.count - 2]) ?? 1
                    listTypes.append(idType)
                }
            }
            if listTypes.count <= 1 {
                listTypes.append(0)
            }
            detailPokemon = DetailPokemon(idPokemon: id, type_1: listTypes[0], type_2: listTypes[1])
            Queries.updateDetailPokemon(pokemondetail: detailPokemon)
        }
        
        if let stats = (data as AnyObject).object(forKey: "stats") as? NSArray{
            statsObj.idPokemon = id
            for item in stats{
                let base_stat = (item as AnyObject).object(forKey: "base_stat") as! Int
                if let stat = (item as AnyObject).object(forKey: "stat") as? NSDictionary{
                    let name = (stat as AnyObject).object(forKey: "name") as! String
                    switch name {
                    case "hp":
                        statsObj.hp = base_stat
                        
                    case "attack":
                        statsObj.attack = base_stat
                        
                    case "defense":
                        statsObj.defense = base_stat
                        
                    case "speed":
                        statsObj.speed = base_stat
                        
                    case "special-attack":
                        statsObj.special_attack = base_stat
                        
                    default:
                        statsObj.special_defense = base_stat
                    }
                }
            }
        }
        
        return (detailPokemon, statsObj)
    }
    
    func ConvertSpeciesPokemon(data: NSDictionary){
           
        var description = ""
        let id = (data as AnyObject).object(forKey: "id") as! Int64
        
           if let texts = (data as AnyObject).object(forKey: "flavor_text_entries") as? NSArray{
               for item in texts{
                   description = (item as AnyObject).object(forKey: "flavor_text") as! String
                if let language = (item as AnyObject).object(forKey: "language") as? NSDictionary{
                    let name = (language as AnyObject).object(forKey: "name") as! String
                    
                    if name == "es"{
                        Queries.updateDescriptionPokemon(id: id, description: description)
                    }
                }
            
               }
           }
           
           
       }
    
    func ConvertTypePokemon(data: NSDictionary){
        
     let id = (data as AnyObject).object(forKey: "id") as! Int64
     let name = (data as AnyObject).object(forKey: "name") as! String
        
        if let names = (data as AnyObject).object(forKey: "names") as? NSArray{
            for item in names{
                let nameLanguage = (item as AnyObject).object(forKey: "name") as! String
                if let language = (item as AnyObject).object(forKey: "language") as? NSDictionary{
                    let nameEs = (language as AnyObject).object(forKey: "name") as! String
                 
                    if nameEs == "es"{
                        let type = TypesDB(idType: id, name: name, name_es: nameLanguage)
                        Queries.insertType(type: type)
                 }
             }
         
            }
        }
        
    }
}
