//
//  TypesDB.swift
//  Pokedex
//
//  Created by Camilo Soto on 13/12/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import Foundation
import GRDB

class TypesDB: Record{
    
    var id: Int64!
    var idType: Int64?
    var name : String?
    var name_es: String?
    
    init(idType : Int64? = nil, name : String? = nil, name_es : String? = nil){
        self.name = name
        self.idType = idType
        self.name = name
        self.name_es = name_es
        super.init()
    }
    
    //MARK: - Record
    override class var databaseTableName: String {
        return TypesDao.NAME_TABLE
    }
    
    
    required init(row: Row) {
        id = row[TypesDao.ID]
        idType = row[TypesDao.ID_TYPE]
        name = row[TypesDao.NAME]
        name_es = row[TypesDao.NAME_ES]
        super.init(row: row)
    }
    
    override func encode(to container: inout PersistenceContainer) {
        container[TypesDao.ID] = id
        container[TypesDao.ID_TYPE] = idType
        container[TypesDao.NAME] = name
        container[TypesDao.NAME_ES] = name_es
    }
    
    
    static func fetchAll(db: Database) -> [TypesDB] {
        return try! TypesDB.fetchAll(db, sql: TypesDao.getSelect())
    }
    
    static func fetchAllWithQuery(_ db: Database, _ query: String) -> [TypesDB] {
        return try! TypesDB.fetchAll(db, sql: TypesDao.getSelectWithQuery(query: query))
    }
    
    static func fetchAllWithOtherQuery(db: Database, query: String) -> [TypesDB]{
        return try! TypesDB.fetchAll(db, sql: TypesDao.getSelectWithQueryNotWhere(query: query))
    }
    
    static func fetchOneWithQuery(_ db: Database, _ query : String) -> TypesDB? {
        return try! TypesDB.fetchOne(db, sql: TypesDao.getSelectWithQuery(query: query))
    }
    
    static func deleteRow(_ db: Database, _ id: Int64) -> TypesDB?{
        return try! TypesDB.fetchOne(db, sql: TypesDao.deleteRow(id: id))
    }
}


