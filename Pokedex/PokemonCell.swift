//
//  PokemonCell.swift
//  Pokedex
//
//  Created by Camilo Soto on 12/12/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import UIKit

class PokemonCell: UITableViewCell {

    @IBOutlet weak var imgPokemon: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var imgType2: UIImageView!
    @IBOutlet weak var imgType1: UIImageView!
    
    var cache = NSCache<NSString, UIImage>()
    var indexRow = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //self.cache = NSCache()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Inicializacion de la celda
    func initData(name: String, urlImage: String, id: Int, isSearch: Bool, indexRow: Int, type_1: Int64, type_2: Int64){
    
        lblName.text = name
        lblNumber.text = String(format: "#%03d", id)
        let key = "\(id)_image"
        let url = URL(string: urlImage)
        
        if let image = self.cache.object(forKey: key as NSString) {
            self.imgPokemon.image = image
            
        }else{
            print("Descargando la imagen # \(key)")
            downloadImage(from: url!, key: key, id: id)
        }
        
        if type_1 != 0{
            imgType1.image = Utilities.getImageType(idType: type_1)
        }
        
        if type_2 != 0{
            imgType2.image = Utilities.getImageType(idType: type_2)
        }
    }
    
    //MARK: Descarga de la imagen de una URL
    func downloadImage(from url: URL, key: String, id: Int) {
        
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            
            DispatchQueue.main.async() {
                self.imgPokemon.image = UIImage(data: data)
                self.cache.setObject(UIImage(data: data) ?? self.imgPokemon.image!, forKey: key as NSString)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }

}
