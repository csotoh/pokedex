//
//  HomePresenter.swift
//  Pokedex
//
//  Created by Camilo Soto on 13/12/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import Foundation

class HomePresenter: HomeContractPresenter, HomeContractCallback{
    
    var view: HomeContractView?
    var interactor: HomeContractInteractor?
    
    init(view: HomeContractView, interactor: HomeContractInteractor) {
        self.view = view
        self.interactor = interactor
        self.interactor?.setCallback(callback: self)
        self.interactor?.getPokemons()
    }
    
    //MARK: Presenter
    func searchPokemon(txtSearch: String) {
        
    }
    
    func getMorePages() {
        
    }
    
    func refreshPage() {
        
    }
    
    //MARK: Callback
    func returnListPokemon(list: [Pokemon], index: Int, isSearch: Bool) {
        self.view?.showPokemon(list: list, index: index, isSearch: isSearch)
    }
    
    func error(description: String) {
        self.view?.showError(error: description)
    }
    
    
}
