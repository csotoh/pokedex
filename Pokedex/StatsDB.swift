//
//  StatsDB.swift
//  Pokedex
//
//  Created by Camilo Soto on 13/12/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import Foundation
import GRDB

class StatsDB: Record{
    
    var id: Int64!
    var idPokemon: Int64?
    var hp : Int?
    var attack: Int?
    var defense: Int?
    var speed: Int?
    var special_attack: Int?
    var special_defense: Int?
    
    init(idPokemon : Int64? = nil, hp : Int? = nil, attack : Int? = nil, defense : Int? = nil, speed : Int? = nil, special_attack : Int? = nil, special_defense : Int? = nil){
        self.hp = hp
        self.idPokemon = idPokemon
        self.attack = attack
        self.defense = defense
        self.speed = speed
        self.special_attack = special_attack
        self.special_defense = special_defense
        super.init()
    }
    
    //MARK: - Record
    override class var databaseTableName: String {
        return StatsDao.NAME_TABLE
    }
    
    
    required init(row: Row) {
        id = row[StatsDao.ID]
        idPokemon = row[StatsDao.ID_POKEMON]
        hp = row[StatsDao.HP]
        attack = row[StatsDao.ATTACK]
        defense = row[StatsDao.DEFENSE]
        speed = row[StatsDao.SPEED]
        special_attack = row[StatsDao.SPECIAL_ATTACK]
        special_defense = row[StatsDao.SPECIAL_DEFENSE]
        super.init(row: row)
    }
    
    override func encode(to container: inout PersistenceContainer) {
        container[StatsDao.ID] = id
        container[StatsDao.ID_POKEMON] = idPokemon
        container[StatsDao.HP] = hp
        container[StatsDao.ATTACK] = attack
        container[StatsDao.DEFENSE] = defense
        container[StatsDao.SPEED] = speed
        container[StatsDao.SPECIAL_ATTACK] = special_attack
        container[StatsDao.SPECIAL_DEFENSE] = special_defense
    }
    
    
    static func fetchAll(db: Database) -> [StatsDB] {
        return try! StatsDB.fetchAll(db, sql: StatsDao.getSelect())
    }
    
    static func fetchAllWithQuery(_ db: Database, _ query: String) -> [StatsDB] {
        return try! StatsDB.fetchAll(db, sql: StatsDao.getSelectWithQuery(query: query))
    }
    
    static func fetchAllWithOtherQuery(db: Database, query: String) -> [StatsDB]{
        return try! StatsDB.fetchAll(db, sql: StatsDao.getSelectWithQueryNotWhere(query: query))
    }
    
    static func fetchOneWithQuery(_ db: Database, _ query : String) -> StatsDB? {
        return try! StatsDB.fetchOne(db, sql: StatsDao.getSelectWithQuery(query: query))
    }
    
    static func deleteRow(_ db: Database, _ id: Int64) -> StatsDB?{
        return try! StatsDB.fetchOne(db, sql: StatsDao.deleteRow(id: id))
    }
}

