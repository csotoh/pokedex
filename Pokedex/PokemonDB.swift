//
//  GenderDB.swift
//  MoviesApp
//
//  Created by Camilo Soto on 7/30/19.
//  Copyright © 2019 NA. All rights reserved.
//

import Foundation
import GRDB

class PokemonDB: Record{
    
    var id: Int64!
    var idPokemon: Int64?
    var name : String?
    var description: String?
    var id_type_1: Int?
    var id_type_2: Int?
    var url_image: String?
    
    init(idPokemon : Int64? = nil, name : String? = nil, description : String? = nil, id_type_1 : Int? = nil, id_type_2 : Int? = nil, url_image : String? = nil){
        self.name = name
        self.idPokemon = idPokemon
        self.name = name
        self.description = description
        self.id_type_1 = id_type_1
        self.id_type_2 = id_type_2
        self.url_image = url_image
        super.init()
    }
    
    //MARK: - Record
    override class var databaseTableName: String {
        return PokemonDao.NAME_TABLE
    }
    
    
    required init(row: Row) {
        id = row[PokemonDao.ID]
        idPokemon = row[PokemonDao.ID_POKEMON]
        name = row[PokemonDao.NAME]
        description = row[PokemonDao.DESCRIPTION]
        id_type_1 = row[PokemonDao.ID_TYPE_1]
        id_type_2 = row[PokemonDao.ID_TYPE_2]
        url_image = row[PokemonDao.URL_IMAGE]
        super.init(row: row)
    }
    
    override func encode(to container: inout PersistenceContainer) {
        container[PokemonDao.ID] = id
        container[PokemonDao.ID_POKEMON] = idPokemon
        container[PokemonDao.NAME] = name
        container[PokemonDao.DESCRIPTION] = description
        container[PokemonDao.ID_TYPE_1] = id_type_1
        container[PokemonDao.ID_TYPE_2] = id_type_1
        container[PokemonDao.URL_IMAGE] = url_image
    }
    
    
    static func fetchAll(db: Database) -> [PokemonDB] {
        return try! PokemonDB.fetchAll(db, sql: PokemonDao.getSelect())
    }
    
    static func fetchAllWithQuery(_ db: Database, _ query: String) -> [PokemonDB] {
        return try! PokemonDB.fetchAll(db, sql: PokemonDao.getSelectWithQuery(query: query))
    }
    
    static func fetchAllWithOtherQuery(db: Database, query: String) -> [PokemonDB]{
        return try! PokemonDB.fetchAll(db, sql: PokemonDao.getSelectWithQueryNotWhere(query: query))
    }
    
    static func fetchOneWithQuery(_ db: Database, _ query : String) -> PokemonDB? {
        return try! PokemonDB.fetchOne(db, sql: PokemonDao.getSelectWithQuery(query: query))
    }
    
    static func deleteRow(_ db: Database, _ id: Int64) -> PokemonDB?{
        return try! PokemonDB.fetchOne(db, sql: PokemonDao.deleteRow(id: id))
    }
}

