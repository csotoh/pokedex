//
//  ViewController.swift
//  Pokedex
//
//  Created by Camilo Soto on 12/12/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    @IBOutlet weak var imgLogo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createTables()
        animation()
    }

    
    func animation(){
        UIView.animate(withDuration: 2.0, animations: {() -> Void in
            self.imgLogo.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 1.0, animations: {() -> Void in
                self.imgLogo.transform = CGAffineTransform(scaleX: 1, y: 1)
                let mainQueue = DispatchQueue.main
                let deadline = DispatchTime.now() + .seconds(3)
                mainQueue.asyncAfter(deadline: deadline) {
                    self.goNextView()
                }
               
               
            })
        })
    }

    func goNextView() {
        performSegue(withIdentifier: "toHome", sender: self)
    }
    
    func createTables(){
        try! PokemonDao.create()
        try! TypesDao.create()
        try! StatsDao.create()
    }

}

