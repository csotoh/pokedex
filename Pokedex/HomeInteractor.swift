//
//  HomeInteractor.swift
//  Pokedex
//
//  Created by Camilo Soto on 13/12/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import Foundation


class HomeInteractor: HomeContractInteractor{
    
    var callback: HomeContractCallback?
    
    var count = 0
    let serviceHandler = ServiceHandler()
    var listPokemon: [Pokemon] = []
    
    //MARK: Interactor
    func getPokemons() {
        count = 0
        serviceHandler.doGetService(callback: callbackListPokemon(isOk:mData:), service: ServiceHandler.Service.list_pokemons, parameters: ["\(count)"])
    }
    
    func getMore() {
        
    }
    
    func searchPokemon(txtSearch: String) {
        
    }
    
    func setCallback(callback: HomeContractCallback) {
        self.callback = callback
    }
    
    func callbackListPokemon(isOk: Bool, mData: [AnyObject]){
        if isOk{
            
            listPokemon = Queries.getAllPokemon()
            
            for pokemon in listPokemon{
                serviceHandler.doGetService(callback: callbackDetailPokemon(isOk:mData:), service: ServiceHandler.Service.pokemon_detail, parameters: ["\(pokemon.id)"])
                serviceHandler.doGetService(callback: callbackSpeciePokemon(isOk:mData:), service: ServiceHandler.Service.specie, parameters: ["\(pokemon.id)"])
                
            }
            callback?.returnListPokemon(list: self.listPokemon, index: 0, isSearch: false)
        }else{
            callback?.error(description: mData[0] as! String)
        }
    }
    
    func callbackDetailPokemon(isOk: Bool, mData: [AnyObject]){
        if isOk{
            
            let pokemonDetail = mData[0] as! (DetailPokemon, StatsDB)
            let detail = pokemonDetail.0
            let stats = pokemonDetail.1
            
            if pokemonDetail.0.type_1 != 0 {
                serviceHandler.doGetService(callback: callbackTypePokemon(isOk:mData:), service: ServiceHandler.Service.type, parameters: ["\(pokemonDetail.0.type_1)"])
            }
            if pokemonDetail.0.type_2 != 0{
                serviceHandler.doGetService(callback: callbackTypePokemon(isOk:mData:), service: ServiceHandler.Service.type, parameters: ["\(pokemonDetail.0.type_2)"])
            }
            
            print("Id:\(detail.idPokemon)   HP: \(String(describing: stats.hp))  ATK: \(String(describing: stats.attack)) DEF: \(String(describing: stats.defense))")
            
        }else{
            callback?.error(description: mData[0] as! String)
        }
    }
    
    func callbackSpeciePokemon(isOk: Bool, mData: [AnyObject]){
        if isOk{
            
        }else{
            callback?.error(description: mData[0] as! String)
        }
    }
    
    func callbackTypePokemon(isOk: Bool, mData: [AnyObject]){
        if isOk{
            
        }else{
            callback?.error(description: mData[0] as! String)
        }
    }
}
