//
//  TypesDao.swift
//  Pokedex
//
//  Created by Camilo Soto on 13/12/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import Foundation
import GRDB

class TypesDao{
    
    static let NAME_TABLE = "TYPES"
    static let ID = "id"
    static let ID_TYPE = "ID_TYPE"
    static let NAME = "NAME"
    static let NAME_ES = "NAME_ES"
    
    //MARK: - Create Entity
    static func create() throws -> Void {
        var migrator = DatabaseMigrator()
        migrator.registerMigration("v1\(NAME_TABLE)") { db in
            
            try db.create(table: NAME_TABLE){ t in
                t.column(ID, .integer).primaryKey()
                t.column(ID_TYPE, .integer)
                t.column(NAME, .text)
                t.column(NAME_ES, .text)
            }
        }
        try! migrator.migrate(DBManager.SharedInstance.getDBQueue())
        
    }
    
    
    static func cleanTable() throws -> Void {
        let queue = DBManager.SharedInstance.getDBQueue()
        try! queue.inTransaction { db in
            try db.execute(sql: "DELETE FROM \(NAME_TABLE)")
            return .commit
        }
    }
    
    static func getSelect() -> String{
        return "SELECT * FROM \(NAME_TABLE)"
    }
    
    static func getSelectWithQuery(query: String) -> String{
        return "\(getSelect()) WHERE \(query)"
    }
    
    static func getSelectWithQueryNotWhere(query: String) -> String{
        return "\(getSelect()) \(query)"
    }
    
    static func deleteRow(id: Int64) -> String {
        return "DELETE FROM \(NAME_TABLE) WHERE id = \(id)"
    }
}
