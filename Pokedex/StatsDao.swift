//
//  StatsDao.swift
//  Pokedex
//
//  Created by Camilo Soto on 13/12/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import Foundation
import GRDB

class StatsDao{
    
    static let NAME_TABLE = "STATS"
    static let ID = "id"
    static let ID_POKEMON = "ID_POKEMON"
    static let HP = "HP"
    static let ATTACK = "ATTACK"
    static let DEFENSE = "DEFENSE"
    static let SPEED = "SPEED"
    static let SPECIAL_ATTACK = "SPECIAL_ATTACK"
    static let SPECIAL_DEFENSE = "SPECIAL_DEFENSE"
    
    //MARK: - Create Entity
    static func create() throws -> Void {
        var migrator = DatabaseMigrator()
        migrator.registerMigration("v1\(NAME_TABLE)") { db in
            
            try db.create(table: NAME_TABLE){ t in
                t.column(ID, .integer).primaryKey()
                t.column(ID_POKEMON, .integer)
                t.column(HP, .text)
                t.column(ATTACK, .text)
                t.column(DEFENSE, .text)
                t.column(SPEED, .text)
                t.column(SPECIAL_ATTACK, .text)
                t.column(SPECIAL_DEFENSE, .text)
            }
        }
        try! migrator.migrate(DBManager.SharedInstance.getDBQueue())
        
    }
    
    
    static func cleanTable() throws -> Void {
        let queue = DBManager.SharedInstance.getDBQueue()
        try! queue.inTransaction { db in
            try db.execute(sql: "DELETE FROM \(NAME_TABLE)")
            return .commit
        }
    }
    
    static func getSelect() -> String{
        return "SELECT * FROM \(NAME_TABLE)"
    }
    
    static func getSelectWithQuery(query: String) -> String{
        return "\(getSelect()) WHERE \(query)"
    }
    
    static func getSelectWithQueryNotWhere(query: String) -> String{
        return "\(getSelect()) \(query)"
    }
    
    static func deleteRow(id: Int64) -> String {
        return "DELETE FROM \(NAME_TABLE) WHERE id = \(id)"
    }
}

