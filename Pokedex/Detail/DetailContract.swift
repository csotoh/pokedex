//
//  DetailContract.swift
//  Pokedex
//
//  Created by Soto Rodriguez on 12/13/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import Foundation
import UIKit

protocol DetailContractView{
    func showInfo(stat: StatsDB, description: String, type: String, imgType: UIImage, name: String)
    func showImagePokemon(img: UIImage)
}

protocol DetailContractPresenter {
    
}

protocol DetailContractInteractor {
    func getInfoPokemon(id: Int64)
    func setCallback(callback: DetailContractCallback)
}

protocol DetailContractCallback {
    func returnInfo(stat: StatsDB, description: String, type: String, imgType: UIImage, name: String)
    func returnImagePokemon(imgPokemon: UIImage)
}
