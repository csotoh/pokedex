//
//  DetailInteractor.swift
//  Pokedex
//
//  Created by Soto Rodriguez on 12/13/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import Foundation
import UIKit

class DetailInteractor: DetailContractInteractor{
    
    var callback: DetailContractCallback?
    let imageDefault = UIImage(named: "pokeball")
    
    //MARK: Interactor
    func getInfoPokemon(id: Int64) {
        let pokemon = Queries.getPokemonForId(id: id)
        let statPokemon = Queries.getStatForID(id: id)
        let type = Queries.getTypeForID(id: Int64((pokemon?.id_type_1!)!))
        let url = URL(string: pokemon?.url_image! ?? "")
        downloadImage(from: url!)
        callback?.returnInfo(stat: statPokemon!, description: (pokemon?.description!)!, type: (type?.name_es!)!, imgType: Utilities.getImageType(idType: Int64((pokemon?.id_type_1!)!)), name: (pokemon?.name!)!)
    }
    
    func setCallback(callback: DetailContractCallback) {
        self.callback = callback
    }
    
    
    
    //MARK: Descarga de la imagen de una URL
    func downloadImage(from url: URL) {
        
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            
            DispatchQueue.main.async() {
                self.callback?.returnImagePokemon(imgPokemon: UIImage(data: data) ?? self.imageDefault!)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
}
