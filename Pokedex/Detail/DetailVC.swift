//
//  DetailVC.swift
//  Pokedex
//
//  Created by Camilo Soto on 12/12/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import UIKit

class DetailVC: UIViewController, DetailContractView{
   

    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imgPokemon: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgType: UIImageView!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var valueHP: UILabel!
    @IBOutlet weak var valueAtk: UILabel!
    @IBOutlet weak var valueDef: UILabel!
    @IBOutlet weak var valueSatk: UILabel!
    @IBOutlet weak var valueSdef: UILabel!
    @IBOutlet weak var valueVel: UILabel!
    @IBOutlet weak var lblNamePokemon: UILabel!
    
    @IBOutlet weak var progressHP: UIProgressView!
    @IBOutlet weak var progressAtk: UIProgressView!
    @IBOutlet weak var progressDef: UIProgressView!
    @IBOutlet weak var progressSatk: UIProgressView!
    @IBOutlet weak var proressSdef: UIProgressView!
    @IBOutlet weak var progressVel: UIProgressView!
    
    
    var presenter: DetailContractPresenter?
    var idPokemon: Int64 = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter = DetailPresenter(view: self, interactor: DetailInteractor(), idPokemon: idPokemon)
    }
    
    //MARK: View
    func showInfo(stat: StatsDB, description: String, type: String, imgType: UIImage, name: String) {
        self.imgType.image = imgType
        self.lblNamePokemon.text = name
        self.lblDescription.text = description
        self.lblType.text = type
        
        self.valueHP.text = String(format: "%03d", stat.hp!)
        self.valueAtk.text = String(format: "%03d", stat.attack!)
        self.valueDef.text = String(format: "%03d", stat.defense!)
        self.valueSatk.text = String(format: "%03d", stat.special_attack!)
        self.valueSdef.text = String(format: "%03d", stat.special_defense!)
        self.valueVel.text = String(format: "%03d", stat.speed!)
       }
       
    func showImagePokemon(img: UIImage) {
        self.imgPokemon.image = img
    }

    @IBAction func backPress(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
}
