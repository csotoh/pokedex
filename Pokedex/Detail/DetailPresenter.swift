//
//  DetailPresenter.swift
//  Pokedex
//
//  Created by Soto Rodriguez on 12/13/19.
//  Copyright © 2019 Csoto. All rights reserved.
//

import Foundation
import UIKit

class DetailPresenter: DetailContractPresenter, DetailContractCallback{
   
    
    
    var view: DetailContractView?
    var interactor: DetailContractInteractor?
    
    init(view: DetailContractView, interactor: DetailContractInteractor, idPokemon: Int64) {
        self.view = view
        self.interactor = interactor
        self.interactor?.setCallback(callback: self)
        self.interactor?.getInfoPokemon(id: idPokemon)
    }
    
    //MARK: Callback
    
    func returnInfo(stat: StatsDB, description: String, type: String, imgType: UIImage, name: String) {
           self.view?.showInfo(stat: stat, description: description, type: type, imgType: imgType, name: name)
       }
       
       func returnImagePokemon(imgPokemon: UIImage) {
        self.view?.showImagePokemon(img: imgPokemon)
       }
    
    
}
