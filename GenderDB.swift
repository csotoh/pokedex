//
//  GenderDB.swift
//  MoviesApp
//
//  Created by Camilo Soto on 7/30/19.
//  Copyright © 2019 NA. All rights reserved.
//

import Foundation
import GRDB

class GenderDB: Record{
    
    var id: Int64!
    var id_gender: Int64!
    var name : String?
    
    init(name : String? = nil, id_gender : Int64? = nil){
        self.name = name
        self.id_gender = id_gender
        super.init()
    }
    
    //MARK: - Record
    override class var databaseTableName: String {
        return GenderDao.NAME_TABLE
    }
    
    
    required init(row: Row) {
        id = row[GenderDao.ID]
        id_gender = row[GenderDao.ID_GENDER]
        name = row[GenderDao.NAME]
        super.init(row: row)
    }
    
    override func encode(to container: inout PersistenceContainer) {
        container[GenderDao.ID] = id
        container[GenderDao.ID_GENDER] = id_gender
        container[GenderDao.NAME] = name
    }
    
    
    static func fetchAll(db: Database) -> [GenderDB] {
        return try! GenderDB.fetchAll(db, sql: GenderDao.getSelect())
    }
    
    static func fetchAllWithQuery(_ db: Database, _ query: String) -> [GenderDB] {
        return try! GenderDB.fetchAll(db, sql: GenderDao.getSelectWithQuery(query: query))
    }
    
    static func fetchAllWithOtherQuery(db: Database, query: String) -> [GenderDB]{
        return try! GenderDB.fetchAll(db, sql: GenderDao.getSelectWithQueryNotWhere(query: query))
    }
    
    static func fetchOneWithQuery(_ db: Database, _ query : String) -> GenderDB? {
        return try! GenderDB.fetchOne(db, sql: GenderDao.getSelectWithQuery(query: query))
    }
    
    static func deleteRow(_ db: Database, _ id: Int64) -> GenderDB?{
        return try! GenderDB.fetchOne(db, sql: GenderDao.deleteRow(id: id))
    }
}

