//
//  GenderDao.swift
//  MoviesApp
//
//  Created by Camilo Soto on 7/30/19.
//  Copyright © 2019 NA. All rights reserved.
//

import Foundation
import GRDB

class GenderDao{
    
    static let NAME_TABLE = "GENDER"
    static let ID = "id"
    static let ID_GENDER = "ID_GENDER"
    static let NAME = "NAME"
    
    //MARK: - Create Entity
    static func create() throws -> Void {
        var migrator = DatabaseMigrator()
        migrator.registerMigration("v1\(NAME_TABLE)") { db in
            
            try db.create(table: NAME_TABLE){ t in
                t.column(ID, .integer).primaryKey()
                t.column(ID_GENDER, .integer)
                t.column(NAME, .text)
            }
        }
        try! migrator.migrate(DBManager.SharedInstance.getDBQueue())
        
    }
    
    
    static func cleanTable() throws -> Void {
        let queue = DBManager.SharedInstance.getDBQueue()
        try! queue.inTransaction { db in
            try db.execute(sql: "DELETE FROM \(NAME_TABLE)")
            return .commit
        }
    }
    
    static func getSelect() -> String{
        return "SELECT * FROM \(NAME_TABLE)"
    }
    
    static func getSelectWithQuery(query: String) -> String{
        return "\(getSelect()) WHERE \(query)"
    }
    
    static func getSelectWithQueryNotWhere(query: String) -> String{
        return "\(getSelect()) \(query)"
    }
    
    static func deleteRow(id: Int64) -> String {
        return "DELETE FROM \(NAME_TABLE) WHERE id = \(id)"
    }
}
